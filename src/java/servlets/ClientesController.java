/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import DAO.ClienteDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entities.ClienteBO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author camilo
 */
public class ClientesController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
            ClienteBO cliente=new ClienteBO();
            ClienteDAO clienteManager=new ClienteDAO();
            switch(request.getParameter("accion")){
		case "crear":
			cliente.setCliRut(request.getParameter("rut"));
			cliente.setCliNombre(request.getParameter("nombre"));
			cliente.setCliApellido(request.getParameter("apellido"));
			if("masculino".equals(request.getParameter("sexo"))){
				cliente.setCliSexo(true);
			}else{
				cliente.setCliSexo(false);
			}
			clienteManager.create(cliente);
			response.sendRedirect("ClienteCrud/Clientes.jsp");
			break;
		case "editar":
			cliente.setCliRut(request.getParameter("rut"));
			cliente.setCliNombre(request.getParameter("nombre"));
			cliente.setCliApellido(request.getParameter("apellido"));
			if("masculino".equals(request.getParameter("sexo"))){
				cliente.setCliSexo(true);
			}else{
				cliente.setCliSexo(false);
			}
			clienteManager.edit(cliente);
			response.sendRedirect("ClienteCrud/Clientes.jsp");
			break;
		case "leer":
		
			cliente=clienteManager.findClienteBO(request.getParameter("rut"));
			request.getSession().setAttribute("rut", cliente.getCliRut());
			request.getSession().setAttribute("nombre", cliente.getCliNombre());
			request.getSession().setAttribute("apellido", cliente.getCliApellido());
			request.getSession().setAttribute("sexo", cliente.getCliSexo()?"Masculino":"Femenino");
			response.sendRedirect("ClienteCrud/EditarCliente.jsp");
			break;
		case "eliminar":
			clienteManager.destroy(request.getParameter("rut"));
			response.sendRedirect("ClienteCrud/Checked.jsp");
			
			break;
		}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
