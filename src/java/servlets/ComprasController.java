/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import DAO.ClienteDAO;
import DAO.DetalleVentaDAO;
import DAO.ProductoDAO;
import DAO.VentaDAO;
import DAO.exceptions.IllegalOrphanException;
import entities.ClienteBO;
import entities.DetalleVentaBO;
import entities.ProductoBO;
import entities.VentaBO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author camilo
 */
public class ComprasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //variables
        Date fecha = new Date();
        List<DetalleVentaBO> carro;
        boolean agregar = true;
        HttpSession sesion = request.getSession();

        //Instancias DAO
        ProductoDAO managerProductos = new ProductoDAO();
        ClienteDAO managerClientes = new ClienteDAO();
        VentaDAO managerVentas = new VentaDAO();
        DetalleVentaDAO managerDetalles = new DetalleVentaDAO();
        
        //acción según boton presionado
        String accion = request.getParameter("accion"); 

        //verifica si hay un carro en session
        if (sesion.getAttribute("carro") == null) {
            carro = new ArrayList<DetalleVentaBO>();
        } else {
            carro = (List<DetalleVentaBO>) sesion.getAttribute("carro");
        }

        DetalleVentaBO detalleVenta;
        VentaBO venta = new VentaBO();;

        switch (accion) {

            case "seleccionCliente":

                String rut = request.getParameter("selectCliente");
                //Buscamos el cliente con el DAO y lo guardamos en un objeto Cliente
                ClienteBO cliente = managerClientes.findClienteBO(rut);
                //Asigna el cliente al objeto venta
                venta.setVenRutCliente(cliente);
                venta.setVenTotal(calcularTotalVenta(carro));//<-- ver metodo calcularTotalVenta más abajo

                //Enviamos el objeto ventay cliente como variable de sesion
                sesion.setAttribute("venta", venta);
                sesion.setAttribute("cliente", cliente);
                response.sendRedirect("Compras/Productos.jsp");
                break;
            case "cancelarCliente":
                //si se cancela la venta, removemos las variables de sesion.
                sesion.removeAttribute("venta");
                sesion.removeAttribute("cliente");
                sesion.removeAttribute("carro");
                response.sendRedirect("Compras/Productos.jsp");
                break;
            case "agregar":
                //Recibe parametros desde el jsp
                String codProducto = request.getParameter("codProd");
                int cantidad = Integer.parseInt(request.getParameter("cantidad"));
                //Busca el producto con el DAO y lo guarda en un objeto Producto
                ProductoBO productoAdd = managerProductos.findProductoBO(codProducto);

                //Recorremos el carro para ver si el producto a agregar ya está dentro
                for (DetalleVentaBO detalle : carro) {
                    //obtiene el id del producto asociado al detalle y lo compara con el que esta entrando
                    if (detalle.getDetCodigoProducto().getProCodigo().equals(productoAdd.getProCodigo())) {
                        agregar = false; //cambiamos el agregar a falso para no repetir el producto
                        break;
                    }
                }

                if (agregar) {
                    //<<<---EL PRODUCTO NO ESTA EN EL CARRO--->>>
                    //Seteamos los datos del detalle de venta
                    detalleVenta = new DetalleVentaBO();
                    detalleVenta.setDetCodigoProducto(productoAdd);
                    detalleVenta.setDetCantidad(cantidad);
                    detalleVenta.setDetPrecioTotal(cantidad * productoAdd.getProPrecio()); //calculo del precio total de ese producto x cantidad
                    carro.add(detalleVenta);
                } else {
                    //<<<---EL PRODUCTO ESTA EN EL CARRO--->>>
                    //recorre el carro buscando el producto
                    for (DetalleVentaBO detalle : carro) {
                        if (detalle.getDetCodigoProducto().getProCodigo().equals(productoAdd.getProCodigo())) {
                            detalle.setDetCantidad(detalle.getDetCantidad() + cantidad); //asignamos la cantidad de productos, cantidadAnterior+cantidad entrante
                            detalle.setDetPrecioTotal(detalle.getDetCantidad() * detalle.getDetCodigoProducto().getProPrecio()); //calculo del precio total de ese producto x cantidad
                            break;
                        }
                    }
                }
                venta.setVenTotal(calcularTotalVenta(carro)); //<-- ver metodo calcularTotalVenta más abajo
                //enviamos las NUEVAS variables venta y carro como variable de sesion
                sesion.setAttribute("venta", venta);
                sesion.setAttribute("carro", carro);
                response.sendRedirect("Compras/Productos.jsp");
                break;
            case "eliminar":
                String codItem = request.getParameter("codItem");
                //iniciamos la posicion en 0, es el inicio del carro
                int posicion = 0;
                //recorremos el carro
                for (DetalleVentaBO detalle : carro) {
                    if (detalle.getDetCodigoProducto().getProCodigo().equals(codItem)) { //<--Buscamos el producto que queremos eliminar
                        //eliminamos el producto según la posicion
                        carro.remove(posicion);
                        break;
                    }
                    posicion++;
                }
                venta.setVenTotal(calcularTotalVenta(carro));//<-- ver metodo calcularTotalVenta más abajo

                //envia NUEVAS variables de venta y el carro como sesion
                sesion.setAttribute("venta", venta);
                sesion.setAttribute("carro", carro);
                response.sendRedirect("Compras/Productos.jsp");
                break;
            case "confirmar":

                //PERSISTENCIA DE VENTA
                venta.setVenRutCliente((ClienteBO) sesion.getAttribute("cliente"));// asigno el rut del cliente
                venta.setVenEstado(true); //asigno el estado de venta
                venta.setVenTotal(calcularTotalVenta(carro)); //asigno el $total de la venta
                venta.setVenFecha(new SimpleDateFormat("dd/MM/yyyy").format(fecha)); //asigno la fecha actual

                managerVentas.create(venta);    //guardo la venta en BBDD, el ID se asigna solo, es tipo SERIAL
                /*
                NOTA: 
                El metodo create, retorna el id que se asignó en la base de dato
                y lo setea dentro del objeto Venta automaticamente
                 */
                //PERSISTENCIA DE DETALLES
                //recorremos el carro de compras
                for (DetalleVentaBO detalle : carro) {
                    //Detalle de venta contiene producto, cantidad y precio total de productos
                    detalle.setDetIdVenta(venta); //revisar nota más arriba
                    managerDetalles.create(detalle);
                }
                //enviamos los NUEVOS datos como variable de sesion
                sesion.removeAttribute("cliente");
                sesion.setAttribute("venta", venta);
                sesion.setAttribute("carro", carro);
                response.sendRedirect("Compras/Checked.jsp");
                break;
        }
    }
    //<<<--- Metodo para calcular el total de la venta --->>>
    private int calcularTotalVenta(List<DetalleVentaBO> detalles) {
        int totalVenta = 0;
        //recorremos el carro de compras
        for (DetalleVentaBO detalle : detalles) {
            //sumamos el total de cada detalle de venta
            totalVenta = totalVenta + detalle.getDetPrecioTotal();
        }
        return totalVenta;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
