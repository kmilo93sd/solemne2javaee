/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.DetalleVentaBO;
import entities.ProductoBO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author camilo
 */
public class ProductoDAO implements Serializable {

    public ProductoDAO() {
        this.emf = Persistence.createEntityManagerFactory("solemne2PU");
    }
    public ProductoDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProductoBO productoBO) throws PreexistingEntityException, Exception {
        if (productoBO.getDetalleVentaBOList() == null) {
            productoBO.setDetalleVentaBOList(new ArrayList<DetalleVentaBO>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<DetalleVentaBO> attachedDetalleVentaBOList = new ArrayList<DetalleVentaBO>();
            for (DetalleVentaBO detalleVentaBOListDetalleVentaBOToAttach : productoBO.getDetalleVentaBOList()) {
                detalleVentaBOListDetalleVentaBOToAttach = em.getReference(detalleVentaBOListDetalleVentaBOToAttach.getClass(), detalleVentaBOListDetalleVentaBOToAttach.getDetId());
                attachedDetalleVentaBOList.add(detalleVentaBOListDetalleVentaBOToAttach);
            }
            productoBO.setDetalleVentaBOList(attachedDetalleVentaBOList);
            em.persist(productoBO);
            for (DetalleVentaBO detalleVentaBOListDetalleVentaBO : productoBO.getDetalleVentaBOList()) {
                ProductoBO oldDetCodigoProductoOfDetalleVentaBOListDetalleVentaBO = detalleVentaBOListDetalleVentaBO.getDetCodigoProducto();
                detalleVentaBOListDetalleVentaBO.setDetCodigoProducto(productoBO);
                detalleVentaBOListDetalleVentaBO = em.merge(detalleVentaBOListDetalleVentaBO);
                if (oldDetCodigoProductoOfDetalleVentaBOListDetalleVentaBO != null) {
                    oldDetCodigoProductoOfDetalleVentaBOListDetalleVentaBO.getDetalleVentaBOList().remove(detalleVentaBOListDetalleVentaBO);
                    oldDetCodigoProductoOfDetalleVentaBOListDetalleVentaBO = em.merge(oldDetCodigoProductoOfDetalleVentaBOListDetalleVentaBO);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProductoBO(productoBO.getProCodigo()) != null) {
                throw new PreexistingEntityException("ProductoBO " + productoBO + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProductoBO productoBO) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductoBO persistentProductoBO = em.find(ProductoBO.class, productoBO.getProCodigo());
            List<DetalleVentaBO> detalleVentaBOListOld = persistentProductoBO.getDetalleVentaBOList();
            List<DetalleVentaBO> detalleVentaBOListNew = productoBO.getDetalleVentaBOList();
            List<String> illegalOrphanMessages = null;
            for (DetalleVentaBO detalleVentaBOListOldDetalleVentaBO : detalleVentaBOListOld) {
                if (!detalleVentaBOListNew.contains(detalleVentaBOListOldDetalleVentaBO)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetalleVentaBO " + detalleVentaBOListOldDetalleVentaBO + " since its detCodigoProducto field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<DetalleVentaBO> attachedDetalleVentaBOListNew = new ArrayList<DetalleVentaBO>();
            for (DetalleVentaBO detalleVentaBOListNewDetalleVentaBOToAttach : detalleVentaBOListNew) {
                detalleVentaBOListNewDetalleVentaBOToAttach = em.getReference(detalleVentaBOListNewDetalleVentaBOToAttach.getClass(), detalleVentaBOListNewDetalleVentaBOToAttach.getDetId());
                attachedDetalleVentaBOListNew.add(detalleVentaBOListNewDetalleVentaBOToAttach);
            }
            detalleVentaBOListNew = attachedDetalleVentaBOListNew;
            productoBO.setDetalleVentaBOList(detalleVentaBOListNew);
            productoBO = em.merge(productoBO);
            for (DetalleVentaBO detalleVentaBOListNewDetalleVentaBO : detalleVentaBOListNew) {
                if (!detalleVentaBOListOld.contains(detalleVentaBOListNewDetalleVentaBO)) {
                    ProductoBO oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO = detalleVentaBOListNewDetalleVentaBO.getDetCodigoProducto();
                    detalleVentaBOListNewDetalleVentaBO.setDetCodigoProducto(productoBO);
                    detalleVentaBOListNewDetalleVentaBO = em.merge(detalleVentaBOListNewDetalleVentaBO);
                    if (oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO != null && !oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO.equals(productoBO)) {
                        oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO.getDetalleVentaBOList().remove(detalleVentaBOListNewDetalleVentaBO);
                        oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO = em.merge(oldDetCodigoProductoOfDetalleVentaBOListNewDetalleVentaBO);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = productoBO.getProCodigo();
                if (findProductoBO(id) == null) {
                    throw new NonexistentEntityException("The productoBO with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductoBO productoBO;
            try {
                productoBO = em.getReference(ProductoBO.class, id);
                productoBO.getProCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The productoBO with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DetalleVentaBO> detalleVentaBOListOrphanCheck = productoBO.getDetalleVentaBOList();
            for (DetalleVentaBO detalleVentaBOListOrphanCheckDetalleVentaBO : detalleVentaBOListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ProductoBO (" + productoBO + ") cannot be destroyed since the DetalleVentaBO " + detalleVentaBOListOrphanCheckDetalleVentaBO + " in its detalleVentaBOList field has a non-nullable detCodigoProducto field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(productoBO);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProductoBO> findProductoBOEntities() {
        return findProductoBOEntities(true, -1, -1);
    }

    public List<ProductoBO> findProductoBOEntities(int maxResults, int firstResult) {
        return findProductoBOEntities(false, maxResults, firstResult);
    }

    private List<ProductoBO> findProductoBOEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProductoBO.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProductoBO findProductoBO(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProductoBO.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoBOCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProductoBO> rt = cq.from(ProductoBO.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
