/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import entities.ClienteBO;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.VentaBO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author camilo
 */
public class ClienteDAO implements Serializable {

    public ClienteDAO() {
        this.emf = Persistence.createEntityManagerFactory("solemne2PU");
    }
    public ClienteDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ClienteBO clienteBO) throws PreexistingEntityException, Exception {
        if (clienteBO.getVentaBOList() == null) {
            clienteBO.setVentaBOList(new ArrayList<VentaBO>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<VentaBO> attachedVentaBOList = new ArrayList<VentaBO>();
            for (VentaBO ventaBOListVentaBOToAttach : clienteBO.getVentaBOList()) {
                ventaBOListVentaBOToAttach = em.getReference(ventaBOListVentaBOToAttach.getClass(), ventaBOListVentaBOToAttach.getVenId());
                attachedVentaBOList.add(ventaBOListVentaBOToAttach);
            }
            clienteBO.setVentaBOList(attachedVentaBOList);
            em.persist(clienteBO);
            for (VentaBO ventaBOListVentaBO : clienteBO.getVentaBOList()) {
                ClienteBO oldVenRutClienteOfVentaBOListVentaBO = ventaBOListVentaBO.getVenRutCliente();
                ventaBOListVentaBO.setVenRutCliente(clienteBO);
                ventaBOListVentaBO = em.merge(ventaBOListVentaBO);
                if (oldVenRutClienteOfVentaBOListVentaBO != null) {
                    oldVenRutClienteOfVentaBOListVentaBO.getVentaBOList().remove(ventaBOListVentaBO);
                    oldVenRutClienteOfVentaBOListVentaBO = em.merge(oldVenRutClienteOfVentaBOListVentaBO);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findClienteBO(clienteBO.getCliRut()) != null) {
                throw new PreexistingEntityException("ClienteBO " + clienteBO + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ClienteBO clienteBO) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ClienteBO persistentClienteBO = em.find(ClienteBO.class, clienteBO.getCliRut());
            List<VentaBO> ventaBOListOld = persistentClienteBO.getVentaBOList();
            List<VentaBO> ventaBOListNew = clienteBO.getVentaBOList();
            List<String> illegalOrphanMessages = null;
            for (VentaBO ventaBOListOldVentaBO : ventaBOListOld) {
                if (!ventaBOListNew.contains(ventaBOListOldVentaBO)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentaBO " + ventaBOListOldVentaBO + " since its venRutCliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<VentaBO> attachedVentaBOListNew = new ArrayList<VentaBO>();
            for (VentaBO ventaBOListNewVentaBOToAttach : ventaBOListNew) {
                ventaBOListNewVentaBOToAttach = em.getReference(ventaBOListNewVentaBOToAttach.getClass(), ventaBOListNewVentaBOToAttach.getVenId());
                attachedVentaBOListNew.add(ventaBOListNewVentaBOToAttach);
            }
            ventaBOListNew = attachedVentaBOListNew;
            clienteBO.setVentaBOList(ventaBOListNew);
            clienteBO = em.merge(clienteBO);
            for (VentaBO ventaBOListNewVentaBO : ventaBOListNew) {
                if (!ventaBOListOld.contains(ventaBOListNewVentaBO)) {
                    ClienteBO oldVenRutClienteOfVentaBOListNewVentaBO = ventaBOListNewVentaBO.getVenRutCliente();
                    ventaBOListNewVentaBO.setVenRutCliente(clienteBO);
                    ventaBOListNewVentaBO = em.merge(ventaBOListNewVentaBO);
                    if (oldVenRutClienteOfVentaBOListNewVentaBO != null && !oldVenRutClienteOfVentaBOListNewVentaBO.equals(clienteBO)) {
                        oldVenRutClienteOfVentaBOListNewVentaBO.getVentaBOList().remove(ventaBOListNewVentaBO);
                        oldVenRutClienteOfVentaBOListNewVentaBO = em.merge(oldVenRutClienteOfVentaBOListNewVentaBO);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = clienteBO.getCliRut();
                if (findClienteBO(id) == null) {
                    throw new NonexistentEntityException("The clienteBO with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ClienteBO clienteBO;
            try {
                clienteBO = em.getReference(ClienteBO.class, id);
                clienteBO.getCliRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The clienteBO with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<VentaBO> ventaBOListOrphanCheck = clienteBO.getVentaBOList();
            for (VentaBO ventaBOListOrphanCheckVentaBO : ventaBOListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ClienteBO (" + clienteBO + ") cannot be destroyed since the VentaBO " + ventaBOListOrphanCheckVentaBO + " in its ventaBOList field has a non-nullable venRutCliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(clienteBO);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ClienteBO> findClienteBOEntities() {
        return findClienteBOEntities(true, -1, -1);
    }

    public List<ClienteBO> findClienteBOEntities(int maxResults, int firstResult) {
        return findClienteBOEntities(false, maxResults, firstResult);
    }

    private List<ClienteBO> findClienteBOEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ClienteBO.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ClienteBO findClienteBO(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ClienteBO.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteBOCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ClienteBO> rt = cq.from(ClienteBO.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
