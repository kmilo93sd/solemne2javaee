/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.NonexistentEntityException;
import entities.DetalleVentaBO;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.ProductoBO;
import entities.VentaBO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author camilo
 */
public class DetalleVentaDAO implements Serializable {

    public DetalleVentaDAO() {
        this.emf = Persistence.createEntityManagerFactory("solemne2PU");
    }
    public DetalleVentaDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetalleVentaBO detalleVentaBO) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductoBO detCodigoProducto = detalleVentaBO.getDetCodigoProducto();
            if (detCodigoProducto != null) {
                detCodigoProducto = em.getReference(detCodigoProducto.getClass(), detCodigoProducto.getProCodigo());
                detalleVentaBO.setDetCodigoProducto(detCodigoProducto);
            }
            VentaBO detIdVenta = detalleVentaBO.getDetIdVenta();
            if (detIdVenta != null) {
                detIdVenta = em.getReference(detIdVenta.getClass(), detIdVenta.getVenId());
                detalleVentaBO.setDetIdVenta(detIdVenta);
            }
            em.persist(detalleVentaBO);
            if (detCodigoProducto != null) {
                detCodigoProducto.getDetalleVentaBOList().add(detalleVentaBO);
                detCodigoProducto = em.merge(detCodigoProducto);
            }
            if (detIdVenta != null) {
                detIdVenta.getDetalleVentaBOList().add(detalleVentaBO);
                detIdVenta = em.merge(detIdVenta);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetalleVentaBO detalleVentaBO) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetalleVentaBO persistentDetalleVentaBO = em.find(DetalleVentaBO.class, detalleVentaBO.getDetId());
            ProductoBO detCodigoProductoOld = persistentDetalleVentaBO.getDetCodigoProducto();
            ProductoBO detCodigoProductoNew = detalleVentaBO.getDetCodigoProducto();
            VentaBO detIdVentaOld = persistentDetalleVentaBO.getDetIdVenta();
            VentaBO detIdVentaNew = detalleVentaBO.getDetIdVenta();
            if (detCodigoProductoNew != null) {
                detCodigoProductoNew = em.getReference(detCodigoProductoNew.getClass(), detCodigoProductoNew.getProCodigo());
                detalleVentaBO.setDetCodigoProducto(detCodigoProductoNew);
            }
            if (detIdVentaNew != null) {
                detIdVentaNew = em.getReference(detIdVentaNew.getClass(), detIdVentaNew.getVenId());
                detalleVentaBO.setDetIdVenta(detIdVentaNew);
            }
            detalleVentaBO = em.merge(detalleVentaBO);
            if (detCodigoProductoOld != null && !detCodigoProductoOld.equals(detCodigoProductoNew)) {
                detCodigoProductoOld.getDetalleVentaBOList().remove(detalleVentaBO);
                detCodigoProductoOld = em.merge(detCodigoProductoOld);
            }
            if (detCodigoProductoNew != null && !detCodigoProductoNew.equals(detCodigoProductoOld)) {
                detCodigoProductoNew.getDetalleVentaBOList().add(detalleVentaBO);
                detCodigoProductoNew = em.merge(detCodigoProductoNew);
            }
            if (detIdVentaOld != null && !detIdVentaOld.equals(detIdVentaNew)) {
                detIdVentaOld.getDetalleVentaBOList().remove(detalleVentaBO);
                detIdVentaOld = em.merge(detIdVentaOld);
            }
            if (detIdVentaNew != null && !detIdVentaNew.equals(detIdVentaOld)) {
                detIdVentaNew.getDetalleVentaBOList().add(detalleVentaBO);
                detIdVentaNew = em.merge(detIdVentaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = detalleVentaBO.getDetId();
                if (findDetalleVentaBO(id) == null) {
                    throw new NonexistentEntityException("The detalleVentaBO with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetalleVentaBO detalleVentaBO;
            try {
                detalleVentaBO = em.getReference(DetalleVentaBO.class, id);
                detalleVentaBO.getDetId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detalleVentaBO with id " + id + " no longer exists.", enfe);
            }
            ProductoBO detCodigoProducto = detalleVentaBO.getDetCodigoProducto();
            if (detCodigoProducto != null) {
                detCodigoProducto.getDetalleVentaBOList().remove(detalleVentaBO);
                detCodigoProducto = em.merge(detCodigoProducto);
            }
            VentaBO detIdVenta = detalleVentaBO.getDetIdVenta();
            if (detIdVenta != null) {
                detIdVenta.getDetalleVentaBOList().remove(detalleVentaBO);
                detIdVenta = em.merge(detIdVenta);
            }
            em.remove(detalleVentaBO);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetalleVentaBO> findDetalleVentaBOEntities() {
        return findDetalleVentaBOEntities(true, -1, -1);
    }

    public List<DetalleVentaBO> findDetalleVentaBOEntities(int maxResults, int firstResult) {
        return findDetalleVentaBOEntities(false, maxResults, firstResult);
    }

    private List<DetalleVentaBO> findDetalleVentaBOEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetalleVentaBO.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetalleVentaBO findDetalleVentaBO(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetalleVentaBO.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetalleVentaBOCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetalleVentaBO> rt = cq.from(DetalleVentaBO.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
