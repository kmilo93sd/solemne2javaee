/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.ClienteBO;
import entities.DetalleVentaBO;
import entities.VentaBO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author camilo
 */
public class VentaDAO implements Serializable {

    public VentaDAO() {
        this.emf = Persistence.createEntityManagerFactory("solemne2PU");
    }
    public VentaDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VentaBO ventaBO) {
        if (ventaBO.getDetalleVentaBOList() == null) {
            ventaBO.setDetalleVentaBOList(new ArrayList<DetalleVentaBO>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ClienteBO venRutCliente = ventaBO.getVenRutCliente();
            if (venRutCliente != null) {
                venRutCliente = em.getReference(venRutCliente.getClass(), venRutCliente.getCliRut());
                ventaBO.setVenRutCliente(venRutCliente);
            }
            List<DetalleVentaBO> attachedDetalleVentaBOList = new ArrayList<DetalleVentaBO>();
            for (DetalleVentaBO detalleVentaBOListDetalleVentaBOToAttach : ventaBO.getDetalleVentaBOList()) {
                detalleVentaBOListDetalleVentaBOToAttach = em.getReference(detalleVentaBOListDetalleVentaBOToAttach.getClass(), detalleVentaBOListDetalleVentaBOToAttach.getDetId());
                attachedDetalleVentaBOList.add(detalleVentaBOListDetalleVentaBOToAttach);
            }
            ventaBO.setDetalleVentaBOList(attachedDetalleVentaBOList);
            em.persist(ventaBO);
            if (venRutCliente != null) {
                venRutCliente.getVentaBOList().add(ventaBO);
                venRutCliente = em.merge(venRutCliente);
            }
            for (DetalleVentaBO detalleVentaBOListDetalleVentaBO : ventaBO.getDetalleVentaBOList()) {
                VentaBO oldDetIdVentaOfDetalleVentaBOListDetalleVentaBO = detalleVentaBOListDetalleVentaBO.getDetIdVenta();
                detalleVentaBOListDetalleVentaBO.setDetIdVenta(ventaBO);
                detalleVentaBOListDetalleVentaBO = em.merge(detalleVentaBOListDetalleVentaBO);
                if (oldDetIdVentaOfDetalleVentaBOListDetalleVentaBO != null) {
                    oldDetIdVentaOfDetalleVentaBOListDetalleVentaBO.getDetalleVentaBOList().remove(detalleVentaBOListDetalleVentaBO);
                    oldDetIdVentaOfDetalleVentaBOListDetalleVentaBO = em.merge(oldDetIdVentaOfDetalleVentaBOListDetalleVentaBO);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VentaBO ventaBO) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaBO persistentVentaBO = em.find(VentaBO.class, ventaBO.getVenId());
            ClienteBO venRutClienteOld = persistentVentaBO.getVenRutCliente();
            ClienteBO venRutClienteNew = ventaBO.getVenRutCliente();
            List<DetalleVentaBO> detalleVentaBOListOld = persistentVentaBO.getDetalleVentaBOList();
            List<DetalleVentaBO> detalleVentaBOListNew = ventaBO.getDetalleVentaBOList();
            List<String> illegalOrphanMessages = null;
            for (DetalleVentaBO detalleVentaBOListOldDetalleVentaBO : detalleVentaBOListOld) {
                if (!detalleVentaBOListNew.contains(detalleVentaBOListOldDetalleVentaBO)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetalleVentaBO " + detalleVentaBOListOldDetalleVentaBO + " since its detIdVenta field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (venRutClienteNew != null) {
                venRutClienteNew = em.getReference(venRutClienteNew.getClass(), venRutClienteNew.getCliRut());
                ventaBO.setVenRutCliente(venRutClienteNew);
            }
            List<DetalleVentaBO> attachedDetalleVentaBOListNew = new ArrayList<DetalleVentaBO>();
            for (DetalleVentaBO detalleVentaBOListNewDetalleVentaBOToAttach : detalleVentaBOListNew) {
                detalleVentaBOListNewDetalleVentaBOToAttach = em.getReference(detalleVentaBOListNewDetalleVentaBOToAttach.getClass(), detalleVentaBOListNewDetalleVentaBOToAttach.getDetId());
                attachedDetalleVentaBOListNew.add(detalleVentaBOListNewDetalleVentaBOToAttach);
            }
            detalleVentaBOListNew = attachedDetalleVentaBOListNew;
            ventaBO.setDetalleVentaBOList(detalleVentaBOListNew);
            ventaBO = em.merge(ventaBO);
            if (venRutClienteOld != null && !venRutClienteOld.equals(venRutClienteNew)) {
                venRutClienteOld.getVentaBOList().remove(ventaBO);
                venRutClienteOld = em.merge(venRutClienteOld);
            }
            if (venRutClienteNew != null && !venRutClienteNew.equals(venRutClienteOld)) {
                venRutClienteNew.getVentaBOList().add(ventaBO);
                venRutClienteNew = em.merge(venRutClienteNew);
            }
            for (DetalleVentaBO detalleVentaBOListNewDetalleVentaBO : detalleVentaBOListNew) {
                if (!detalleVentaBOListOld.contains(detalleVentaBOListNewDetalleVentaBO)) {
                    VentaBO oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO = detalleVentaBOListNewDetalleVentaBO.getDetIdVenta();
                    detalleVentaBOListNewDetalleVentaBO.setDetIdVenta(ventaBO);
                    detalleVentaBOListNewDetalleVentaBO = em.merge(detalleVentaBOListNewDetalleVentaBO);
                    if (oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO != null && !oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO.equals(ventaBO)) {
                        oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO.getDetalleVentaBOList().remove(detalleVentaBOListNewDetalleVentaBO);
                        oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO = em.merge(oldDetIdVentaOfDetalleVentaBOListNewDetalleVentaBO);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ventaBO.getVenId();
                if (findVentaBO(id) == null) {
                    throw new NonexistentEntityException("The ventaBO with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentaBO ventaBO;
            try {
                ventaBO = em.getReference(VentaBO.class, id);
                ventaBO.getVenId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventaBO with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DetalleVentaBO> detalleVentaBOListOrphanCheck = ventaBO.getDetalleVentaBOList();
            for (DetalleVentaBO detalleVentaBOListOrphanCheckDetalleVentaBO : detalleVentaBOListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This VentaBO (" + ventaBO + ") cannot be destroyed since the DetalleVentaBO " + detalleVentaBOListOrphanCheckDetalleVentaBO + " in its detalleVentaBOList field has a non-nullable detIdVenta field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            ClienteBO venRutCliente = ventaBO.getVenRutCliente();
            if (venRutCliente != null) {
                venRutCliente.getVentaBOList().remove(ventaBO);
                venRutCliente = em.merge(venRutCliente);
            }
            em.remove(ventaBO);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VentaBO> findVentaBOEntities() {
        return findVentaBOEntities(true, -1, -1);
    }

    public List<VentaBO> findVentaBOEntities(int maxResults, int firstResult) {
        return findVentaBOEntities(false, maxResults, firstResult);
    }

    private List<VentaBO> findVentaBOEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VentaBO.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VentaBO findVentaBO(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VentaBO.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaBOCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VentaBO> rt = cq.from(VentaBO.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
