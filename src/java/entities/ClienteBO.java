/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author camilo
 */
@Entity
@Table(name = "tbl_clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClienteBO.findAll", query = "SELECT c FROM ClienteBO c")
    , @NamedQuery(name = "ClienteBO.findByCliRut", query = "SELECT c FROM ClienteBO c WHERE c.cliRut = :cliRut")
    , @NamedQuery(name = "ClienteBO.findByCliNombre", query = "SELECT c FROM ClienteBO c WHERE c.cliNombre = :cliNombre")
    , @NamedQuery(name = "ClienteBO.findByCliApellido", query = "SELECT c FROM ClienteBO c WHERE c.cliApellido = :cliApellido")
    , @NamedQuery(name = "ClienteBO.findByCliSexo", query = "SELECT c FROM ClienteBO c WHERE c.cliSexo = :cliSexo")})
public class ClienteBO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cli_rut")
    private String cliRut;
    @Basic(optional = false)
    @Column(name = "cli_nombre")
    private String cliNombre;
    @Basic(optional = false)
    @Column(name = "cli_apellido")
    private String cliApellido;
    @Basic(optional = false)
    @Column(name = "cli_sexo")
    private boolean cliSexo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "venRutCliente")
    private List<VentaBO> ventaBOList;

    public ClienteBO() {
    }

    public ClienteBO(String cliRut) {
        this.cliRut = cliRut;
    }

    public ClienteBO(String cliRut, String cliNombre, String cliApellido, boolean cliSexo) {
        this.cliRut = cliRut;
        this.cliNombre = cliNombre;
        this.cliApellido = cliApellido;
        this.cliSexo = cliSexo;
    }

    public String getCliRut() {
        return cliRut;
    }

    public void setCliRut(String cliRut) {
        this.cliRut = cliRut;
    }

    public String getCliNombre() {
        return cliNombre;
    }

    public void setCliNombre(String cliNombre) {
        this.cliNombre = cliNombre;
    }

    public String getCliApellido() {
        return cliApellido;
    }

    public void setCliApellido(String cliApellido) {
        this.cliApellido = cliApellido;
    }

    public boolean getCliSexo() {
        return cliSexo;
    }

    public void setCliSexo(boolean cliSexo) {
        this.cliSexo = cliSexo;
    }

    @XmlTransient
    public List<VentaBO> getVentaBOList() {
        return ventaBOList;
    }

    public void setVentaBOList(List<VentaBO> ventaBOList) {
        this.ventaBOList = ventaBOList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliRut != null ? cliRut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteBO)) {
            return false;
        }
        ClienteBO other = (ClienteBO) object;
        if ((this.cliRut == null && other.cliRut != null) || (this.cliRut != null && !this.cliRut.equals(other.cliRut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ClienteBO[ cliRut=" + cliRut + " ]";
    }
    
}
