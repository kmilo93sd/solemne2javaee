/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author camilo
 */
@Entity
@Table(name = "tbl_productos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoBO.findAll", query = "SELECT p FROM ProductoBO p")
    , @NamedQuery(name = "ProductoBO.findByProCodigo", query = "SELECT p FROM ProductoBO p WHERE p.proCodigo = :proCodigo")
    , @NamedQuery(name = "ProductoBO.findByProNombre", query = "SELECT p FROM ProductoBO p WHERE p.proNombre = :proNombre")
    , @NamedQuery(name = "ProductoBO.findByProPrecio", query = "SELECT p FROM ProductoBO p WHERE p.proPrecio = :proPrecio")})
public class ProductoBO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pro_codigo")
    private String proCodigo;
    @Basic(optional = false)
    @Column(name = "pro_nombre")
    private String proNombre;
    @Basic(optional = false)
    @Column(name = "pro_precio")
    private int proPrecio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detCodigoProducto")
    private List<DetalleVentaBO> detalleVentaBOList;

    public ProductoBO() {
    }

    public ProductoBO(String proCodigo) {
        this.proCodigo = proCodigo;
    }

    public ProductoBO(String proCodigo, String proNombre, int proPrecio) {
        this.proCodigo = proCodigo;
        this.proNombre = proNombre;
        this.proPrecio = proPrecio;
    }

    public String getProCodigo() {
        return proCodigo;
    }

    public void setProCodigo(String proCodigo) {
        this.proCodigo = proCodigo;
    }

    public String getProNombre() {
        return proNombre;
    }

    public void setProNombre(String proNombre) {
        this.proNombre = proNombre;
    }

    public int getProPrecio() {
        return proPrecio;
    }

    public void setProPrecio(int proPrecio) {
        this.proPrecio = proPrecio;
    }

    @XmlTransient
    public List<DetalleVentaBO> getDetalleVentaBOList() {
        return detalleVentaBOList;
    }

    public void setDetalleVentaBOList(List<DetalleVentaBO> detalleVentaBOList) {
        this.detalleVentaBOList = detalleVentaBOList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proCodigo != null ? proCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoBO)) {
            return false;
        }
        ProductoBO other = (ProductoBO) object;
        if ((this.proCodigo == null && other.proCodigo != null) || (this.proCodigo != null && !this.proCodigo.equals(other.proCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductoBO[ proCodigo=" + proCodigo + " ]";
    }
    
}
