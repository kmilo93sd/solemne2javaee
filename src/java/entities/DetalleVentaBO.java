/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author camilo
 */
@Entity
@Table(name = "tbl_detalles_ventas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleVentaBO.findAll", query = "SELECT d FROM DetalleVentaBO d")
    , @NamedQuery(name = "DetalleVentaBO.findByDetCantidad", query = "SELECT d FROM DetalleVentaBO d WHERE d.detCantidad = :detCantidad")
    , @NamedQuery(name = "DetalleVentaBO.findByDetPrecioTotal", query = "SELECT d FROM DetalleVentaBO d WHERE d.detPrecioTotal = :detPrecioTotal")
    , @NamedQuery(name = "DetalleVentaBO.findByDetId", query = "SELECT d FROM DetalleVentaBO d WHERE d.detId = :detId")})
public class DetalleVentaBO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "det_cantidad")
    private int detCantidad;
    @Basic(optional = false)
    @Column(name = "det_precio_total")
    private int detPrecioTotal;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "det_id")
    private Integer detId;
    @JoinColumn(name = "det_codigo_producto", referencedColumnName = "pro_codigo")
    @ManyToOne(optional = false)
    private ProductoBO detCodigoProducto;
    @JoinColumn(name = "det_id_venta", referencedColumnName = "ven_id")
    @ManyToOne(optional = false)
    private VentaBO detIdVenta;

    public DetalleVentaBO() {
    }

    public DetalleVentaBO(Integer detId) {
        this.detId = detId;
    }

    public DetalleVentaBO(Integer detId, int detCantidad, int detPrecioTotal) {
        this.detId = detId;
        this.detCantidad = detCantidad;
        this.detPrecioTotal = detPrecioTotal;
    }

    public int getDetCantidad() {
        return detCantidad;
    }

    public void setDetCantidad(int detCantidad) {
        this.detCantidad = detCantidad;
    }

    public int getDetPrecioTotal() {
        return detPrecioTotal;
    }

    public void setDetPrecioTotal(int detPrecioTotal) {
        this.detPrecioTotal = detPrecioTotal;
    }

    public Integer getDetId() {
        return detId;
    }

    public void setDetId(Integer detId) {
        this.detId = detId;
    }

    public ProductoBO getDetCodigoProducto() {
        return detCodigoProducto;
    }

    public void setDetCodigoProducto(ProductoBO detCodigoProducto) {
        this.detCodigoProducto = detCodigoProducto;
    }

    public VentaBO getDetIdVenta() {
        return detIdVenta;
    }

    public void setDetIdVenta(VentaBO detIdVenta) {
        this.detIdVenta = detIdVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detId != null ? detId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleVentaBO)) {
            return false;
        }
        DetalleVentaBO other = (DetalleVentaBO) object;
        if ((this.detId == null && other.detId != null) || (this.detId != null && !this.detId.equals(other.detId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.DetalleVentaBO[ detId=" + detId + " ]";
    }
    
}
