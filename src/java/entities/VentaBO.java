/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author camilo
 */
@Entity
@Table(name = "tbl_ventas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaBO.findAll", query = "SELECT v FROM VentaBO v")
    , @NamedQuery(name = "VentaBO.findByVenId", query = "SELECT v FROM VentaBO v WHERE v.venId = :venId")
    , @NamedQuery(name = "VentaBO.findByVenFecha", query = "SELECT v FROM VentaBO v WHERE v.venFecha = :venFecha")
    , @NamedQuery(name = "VentaBO.findByVenEstado", query = "SELECT v FROM VentaBO v WHERE v.venEstado = :venEstado")
    , @NamedQuery(name = "VentaBO.findByVenTotal", query = "SELECT v FROM VentaBO v WHERE v.venTotal = :venTotal")})
public class VentaBO implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ven_id")
    private Integer venId;
    @Basic(optional = false)
    @Column(name = "ven_fecha")
    private String venFecha;
    @Basic(optional = false)
    @Column(name = "ven_estado")
    private boolean venEstado;
    @Basic(optional = false)
    @Column(name = "ven_total")
    private int venTotal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detIdVenta")
    private List<DetalleVentaBO> detalleVentaBOList;
    @JoinColumn(name = "ven_rut_cliente", referencedColumnName = "cli_rut")
    @ManyToOne(optional = false)
    private ClienteBO venRutCliente;

    public VentaBO() {
    }

    public VentaBO(Integer venId) {
        this.venId = venId;
    }

    public VentaBO(Integer venId, String venFecha, boolean venEstado, int venTotal) {
        this.venId = venId;
        this.venFecha = venFecha;
        this.venEstado = venEstado;
        this.venTotal = venTotal;
    }

    public Integer getVenId() {
        return venId;
    }

    public void setVenId(Integer venId) {
        this.venId = venId;
    }

    public String getVenFecha() {
        return venFecha;
    }

    public void setVenFecha(String venFecha) {
        this.venFecha = venFecha;
    }

    public boolean getVenEstado() {
        return venEstado;
    }

    public void setVenEstado(boolean venEstado) {
        this.venEstado = venEstado;
    }

    public int getVenTotal() {
        return venTotal;
    }

    public void setVenTotal(int venTotal) {
        this.venTotal = venTotal;
    }

    @XmlTransient
    public List<DetalleVentaBO> getDetalleVentaBOList() {
        return detalleVentaBOList;
    }

    public void setDetalleVentaBOList(List<DetalleVentaBO> detalleVentaBOList) {
        this.detalleVentaBOList = detalleVentaBOList;
    }

    public ClienteBO getVenRutCliente() {
        return venRutCliente;
    }

    public void setVenRutCliente(ClienteBO venRutCliente) {
        this.venRutCliente = venRutCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (venId != null ? venId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaBO)) {
            return false;
        }
        VentaBO other = (VentaBO) object;
        if ((this.venId == null && other.venId != null) || (this.venId != null && !this.venId.equals(other.venId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VentaBO[ venId=" + venId + " ]";
    }
    
}
