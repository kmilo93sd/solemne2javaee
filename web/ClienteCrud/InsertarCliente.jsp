<%-- 
    Document   : InsertarCliente
    Created on : 29-nov-2017, 16:06:31
    Author     : camilo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>Ingresar Cliente</h1>
            <form action="../ClientesController" method="POST">
			<label>Rut</label><input type="text" name="rut"><br>
			<label>Nombre</label><input type="text" name="nombre"><br>
			<label>Apellido</label><input type="text" name="apellido"><br>
			<label>Sexo</label>
			<label class="radio-inline"><input type="radio" name="sexo" value="masculino">Masculino</label>
			<label class="radio-inline"><input type="radio" name="sexo" value="femenino">Femenino</label><br>
			<button type="submit" class="btn btn-primary" name="accion" value="crear">Crear</button>
		</form>
        </div>
        
    </body>
</html>
