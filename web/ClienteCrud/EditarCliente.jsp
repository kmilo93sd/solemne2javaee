<%-- 
    Document   : EditarUsuario
    Created on : 29-nov-2017, 16:46:56
    Author     : camilo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>Editar Cliente</h1>
            <form action="../ClientesController" method="POST">
			<table class="table">
	 			<tbody>
	    			<tr>
	      				<td><label>Rut</label></td>
	      				<td><label><%=session.getAttribute("rut") %></label><input type="hidden" name="rut" value="<%=session.getAttribute("rut") %>"></td>
	    			</tr>
	    			<tr>
	      				<td><label>Nombre</label></td>
	      				<td><input type="text" name="nombre"  value="<%=session.getAttribute("nombre") %>"></td>
	    			</tr>
			    	<tr>
			      		<td><label>Apellido</label></td>
			      		<td><input type="text" name="apellido"  value="<%=session.getAttribute("apellido") %>"></td>
			    	</tr>
			    	<tr>
			      		<td><label>Sexo</label></td>
			      		<td><%=session.getAttribute("sexo") %><br>
			      			<label class="radio-inline"><input type="radio" name="sexo" value="masculino">Masculino</label>
							<label class="radio-inline"><input type="radio" name="sexo" value="femenino">Femenino</label>
			      		</td>
			    	</tr>
			    </tbody>
		    </table>
			
			<br>
			<br>
			
			
			<br>
			<button type="submit" class="btn btn-primary" name="accion" value="editar">Editar</button>
			<a class="btn btn-primary" href="Clientes.jsp" role="button">Salir</a>
		</form>
        </div>
        
    </body>
</html>
