<%-- 
    Document   : Clientes
    Created on : 29-nov-2017, 16:21:40
    Author     : camilo
--%>

<%@page import="DAO.ClienteDAO"%>
<%@page import="java.util.List"%>
<%@page import="entities.ClienteBO"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<h2>Gestion Clientes</h2>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">

			<div class="col-md-5">
				<h2>Leer</h2>
				<p>
				<form action="../ClientesController" method="POST">
					<input type="text" name="rut" placeholder="rut">
					<button type="submit" class="btn btn-primary" name="accion"
						value="leer">Leer</button>
				</form>
				<p>
					<a class="btn btn-primary" href="InsertarCliente.jsp" role="button">Nuevo
						Cliente</a>
				<p>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-6">
				<table class="table">
					<thead>
						<tr>
							<th>Rut</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Sexo</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<%
							List<ClienteBO> listado = new ArrayList<ClienteBO>();
							ClienteDAO objeto = new ClienteDAO();
							listado = objeto.findClienteBOEntities();
							for (ClienteBO elemento : listado) {
						%>
						<tr>
							<td><%=elemento.getCliRut()%></td>
							<td><%=elemento.getCliNombre() %></td>
							<td><%=elemento.getCliApellido() %></td>
							<td><%=elemento.getCliSexo() ? "Masculino" : "Femenino"%></td>
							<td>
								<form action="../ClientesController" method="POST">
									<input type="hidden" name="rut" value="<%=elemento.getCliRut() %>">
									<button type="submit" class="btn btn-primary" name="accion"
										value="leer">Leer</button>
								</form>
							</td>
							<td>
								<form action="../ClientesController" method="POST">
									<input type="hidden" name="rut" value="<%=elemento.getCliRut()%>">
									<button type="submit" class="btn btn-danger" name="accion"
										value="eliminar">Eliminar</button>
								</form>
								
							</td>
						</tr>
						<%
							}
						%>






					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>

		<a class="btn btn-primary" href="../Index.jsp" role="button">Salir</a>
	</div>
        
    </body>
</html>
