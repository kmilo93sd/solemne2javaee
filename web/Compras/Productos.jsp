<%-- 
    Document   : Compras
    Created on : 29-nov-2017, 16:19:16
    Author     : camilo
--%>

<%@page import="entities.VentaBO"%>
<%@page import="entities.ClienteBO"%>
<%@page import="DAO.ClienteDAO"%>
<%@page import="entities.DetalleVentaBO"%>
<%@page import="java.util.List"%>
<%@page import="DAO.ProductoDAO"%>
<%@page import="entities.ProductoBO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-6">
                    <h1>Gestion de Compras</h1>
                </div>
                <div class="col-4"></div>
            </div>

            <div class="row">
                <div class="col-2"></div>
                <div class="col-5">
                    <div class="row">

                        <form action="../ComprasController" method="POST">
                            <div class="form-inline">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <%
                                                //revisamos si hay cliente asignado en sesion
                                                if (session.getAttribute("cliente") == null) {
                                                    out.print("<label for='selectCliente'><h3>Selecciona un cliente:</h3></label>");
                                                    out.print("<td>");
                                                    out.print("<input type='text' name='selectCliente'");
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print("<button type='submit' class='btn btn-primary btn-xs' name='accion' value='seleccionCliente'>Seleccionar</button>");
                                                    out.print("</td>");
                                                } else {
                                                    //tomamos el cliente de la sesion y lo asignamos a un objeto Cliente
                                                    ClienteBO cliente = (ClienteBO) session.getAttribute("cliente");
                                                    out.print("<td>");
                                                    out.print("<h5><span class='label label-default'>Cliente: " + cliente.getCliRut() + ", " + cliente.getCliNombre() + " " + cliente.getCliApellido() + "</span></h5>");
                                                    out.print("</td>");
                                                    out.print("<td>");
                                                    out.print("<button type='submit' class='btn btn-primary btn-xs' name='accion' value='cancelarCliente'>Cancelar</button>");
                                                    out.print("</td>");
                                                }
                                            %>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </form>

                    </div>
                    <%
                        //<<<---SOLO VISIBLE CUANDO HAY CLIENTE--->>>
                        
                        //revisa si hay cliente en sesion
                        if (session.getAttribute("cliente") != null) {

                            out.println("<table class='table'>");
                            out.println("<thead>");
                            out.println("<tr>");
                            out.println("<td>Producto</td>");
                            out.println("<td>Precio</td>");
                            out.println("<td>Cantidad</td>");
                            out.println("<td>Agregar</td>");
                            out.println("</tr>");
                            out.println("</thead>");
                            out.println("<tbody>");
                            ProductoDAO managerProductos = new ProductoDAO();
                            //recibimos la lista de productos desde el DAO
                            List<ProductoBO> listadoProductos = managerProductos.findProductoBOEntities();
                            for (ProductoBO producto : listadoProductos) {
                                out.println("<form action='../ComprasController' method='POST'>");
                                out.println("<tr>");
                                out.println("<td>" + producto.getProNombre() + "</td>");
                                out.println("<td>" + producto.getProPrecio() + "</td>");
                                out.println("<td><input type='text' name='cantidad' value='1'></td>");
                                out.println("<td><input type='hidden' name='codProd' value='" + producto.getProCodigo() + "'><button type='submit' class='btn btn-primary btn-xs' name='accion' value='agregar'>+</button></td>");
                                out.println("</tr>");
                                out.println("</form>");
                            }
                            out.println("</tbody>");
                            out.println("</table>");
                            out.println("</div>");
                            out.println("<div class='col-1'></div>");
                            out.println("<div class='col-4'>");
                            
                            //<<<---SOLO ES VISIBLE SI HAY UN CARRO CREADO--->>>
                            //verificamos si hay un carro en sesion
                            if (session.getAttribute("carro") != null) {
                                //asigna el carro de sesion a una lista de objetos Detalles
                                List<DetalleVentaBO> carro = (List<DetalleVentaBO>) session.getAttribute("carro");
                                //asigna la venta de sesion a un objeto Venta
                                VentaBO venta = (VentaBO) session.getAttribute("venta");
                                //verifica si el carro tiene items
                                if (!carro.isEmpty()) {

                                    out.println("<table class='table'>");
                                    out.println("<thead>");
                                    out.println("<tr>");
                                    out.println("<td>Producto</td>");
                                    out.println("<td>Cantidad</td>");
                                    out.println("<td>Precio Total</td>");
                                    out.println("<td>Eliminar</td>");
                                    out.println("</tr>");
                                    out.println("</thead>");
                                    out.println("<tbody>");

                                    out.println("<h1>Tu carro</h1>");
                                    for (DetalleVentaBO detalle : carro) {
                                        out.println("<form action='../ComprasController' method='POST'>");
                                        out.println("<tr>");
                                        out.println("<td>" + detalle.getDetCodigoProducto().getProNombre() + "</td>");
                                        out.println("<td>" + detalle.getDetCantidad() + "</td>");
                                        out.println("<td>" + detalle.getDetPrecioTotal() + "</td>");
                                        out.println("<td><input type='hidden' name='codItem' value='" + detalle.getDetCodigoProducto().getProCodigo() + "'><button type='submit' class='btn btn-primary btn-xs' name='accion' value='eliminar'>-</button></td>");
                                        out.println("</tr>");
                                        out.println("</form>");
                                    }
                                    out.println("<tr>");
                                    out.println("<form action='../ComprasController' method='POST'>");
                                    out.println("<td></td>");
                                    out.println("<td>Total </td>");
                                    out.println("<td>" + venta.getVenTotal() + "</td>");
                                    out.println("<td><button type='submit' class='btn btn-primary btn-xs' name='accion' value='confirmar'>Comprar</button></td>");
                                    out.println("</form>");
                                    out.println("</tr>");
                                    out.println("</tbody>");
                                    out.println("</table>");

                                }
                            }
                        }
                    %>


                </div>
            </div>
        </form>
        <a class="btn btn-primary" href="../Index.jsp" role="button">Salir</a>

</body>
</html>
