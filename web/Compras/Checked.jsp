<%-- 
    Document   : Checked
    Created on : 06-dic-2017, 17:24:30
    Author     : camilo
--%>

<%@page import="java.util.List"%>
<%@page import="entities.DetalleVentaBO"%>
<%@page import="entities.VentaBO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="container">
            <h1>Detalle Boleta</h1>
            <%
                VentaBO venta = (VentaBO) session.getAttribute("venta");
                List<DetalleVentaBO> carro = (List<DetalleVentaBO>) session.getAttribute("carro");
                out.print("<span class='label label-default'>Fecha: </span> " + venta.getVenFecha()+"</br>");
                out.print("<span class='label label-default'>Rut Cliente: </span> " + venta.getVenRutCliente().getCliRut()+"</br>");
                out.print("<span class='label label-default'>Cliente: </span> " + venta.getVenRutCliente().getCliNombre() + " " + venta.getVenRutCliente().getCliApellido()+"</br>");
                out.println("<table class='table'>");
                out.println("<thead>");
                out.println("<tr>");
                out.println("<td>Producto</td>");
                out.println("<td>Cantidad</td>");
                out.println("<td>Valor</td>");
                out.println("</tr>");
                out.println("</thead>");
                out.println("<tbody>");
                for (DetalleVentaBO detalle : carro) {
                    out.println("<tr>");
                    out.println("<td>" + detalle.getDetCodigoProducto().getProNombre() + "</td>");
                    out.println("<td>" + detalle.getDetCantidad() + "</td>");
                    out.println("<td>" + detalle.getDetPrecioTotal() + "</td>");
                    out.println("</tr>");
                }
                out.println("<tr>");
                    out.println("<td></td>");
                    out.println("<td>TOTAL:</td>");
                    out.println("<td>" + venta.getVenTotal() + "</td>");
                    out.println("</tr>");
                out.println("</tbody>");
                out.println("</table>");
                session.removeAttribute("venta");
                session.removeAttribute("carro");
            %>
            
            <a class="btn btn-primary" href="Productos.jsp" role="button">Salir</a>
        </div>
    </body>
</html>
